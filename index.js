import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

var blogPosts = [
    {
        name: "Henry",
        blogTimestamp: "09/12/17",
        profileImage: "../images/henry.jpg",
        postContent: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel nisl quis felis placerat bibendum. Vestibulum rutrum lacus ultricies, venenatis tortor tristique, bibendum metus. Integer iaculis vel ligula ut maximus. Sed mattis massa suscipit velit efficitur, non vestibulum orci tempor. Quisque dolor velit, porta quis dignissim vel, aliquet vel lacus. Suspendisse tincidunt libero quis odio condimentum, et porta orci fermentum. Nulla fermentum leo neque, sit amet faucibus orci euismod vitae. Fusce sed interdum dui. Cras molestie sagittis maximus. Nullam interdum accumsan mauris eget vestibulum. Ut mollis pretium interdum. Proin malesuada eleifend lacus pulvinar luctus. ",
        likes: 12,
        liked: true,
        blogComments: [
            {name: "Joe", profileImage: "../images/joe.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "}
        ]
    },

    {
        name: "Joe",
        blogTimestamp: "09/01/17",
        profileImage: "../images/joe.jpg",
        postContent: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel nisl quis felis placerat bibendum. Vestibulum rutrum lacus ultricies, venenatis tortor tristique, bibendum metus. Integer iaculis vel ligula ut maximus. Sed mattis massa suscipit velit efficitur, non vestibulum orci tempor. Quisque dolor velit, porta quis dignissim vel, aliquet vel lacus. Suspendisse tincidunt libero quis odio condimentum, et porta orci fermentum. Nulla fermentum leo neque, sit amet faucibus orci euismod vitae. Fusce sed interdum dui. Cras molestie sagittis maximus. Nullam interdum accumsan mauris eget vestibulum. Ut mollis pretium interdum. Proin malesuada eleifend lacus pulvinar luctus. ",
        likes: 0,
        liked: false,
        blogComments: [
            {name: "Jane", profileImage: "../images/jane.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "},
            {name: "Ben", profileImage: "../images/ben.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "}
        ]
    },
    {
        name: "Jane",
        blogTimestamp: "07/18/17",
        profileImage: "../images/jane.jpg",
        postContent: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel nisl quis felis placerat bibendum. Vestibulum rutrum lacus ultricies, venenatis tortor tristique, bibendum metus. Integer iaculis vel ligula ut maximus. Sed mattis massa suscipit velit efficitur, non vestibulum orci tempor. Quisque dolor velit, porta quis dignissim vel, aliquet vel lacus. Suspendisse tincidunt libero quis odio condimentum, et porta orci fermentum. Nulla fermentum leo neque, sit amet faucibus orci euismod vitae. Fusce sed interdum dui. Cras molestie sagittis maximus. Nullam interdum accumsan mauris eget vestibulum. Ut mollis pretium interdum. Proin malesuada eleifend lacus pulvinar luctus. ",
        likes: 97,
        liked: true,
        blogComments: [
            {name: "Joe", profileImage: "../images/joe.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "}
        ]
    },

    {
        name: "Ben",
        blogTimestamp: "07/07/17",
        profileImage: "../images/ben.jpg",
        postContent: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel nisl quis felis placerat bibendum. Vestibulum rutrum lacus.",
        likes: 74,
        liked: false,
        blogComments: [
            {name: "Jane", profileImage: "../images/jane.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "},
            {name: "Ben", profileImage: "../images/ben.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "}
        ]
    },
    {
        name: "Henry",
        blogTimestamp: "06/12/17",
        profileImage: "../images/henry.jpg",
        postContent: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel nisl quis felis placerat bibendum. Vestibulum rutrum lacus ultricies, venenatis tortor tristique, bibendum metus. Integer iaculis vel ligula ut maximus. Sed mattis massa suscipit velit efficitur, non vestibulum orci tempor. Quisque dolor velit, porta quis dignissim vel, aliquet vel lacus. Suspendisse tincidunt libero quis odio condimentum, et porta orci fermentum. Nulla fermentum leo neque, sit amet faucibus orci euismod vitae. Fusce sed interdum dui. Cras molestie sagittis maximus. Nullam interdum accumsan mauris eget vestibulum. Ut mollis pretium interdum. Proin malesuada eleifend lacus pulvinar luctus. ",
        likes: 71,
        liked: false,
        blogComments: [
            {name: "Joe", profileImage: "../images/joe.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "}
        ]
    },

    {
        name: "Joe",
        blogTimestamp: "06/08/17",
        profileImage: "../images/joe.jpg",
        postContent: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel nisl quis felis placerat bibendum. Vestibulum rutrum lacus ultricies, venenatis tortor tristique, bibendum metus. Integer iaculis vel ligula ut maximus. Sed mattis massa suscipit velit efficitur, non vestibulum orci tempor. Quisque dolor velit, porta quis dignissim vel, aliquet vel lacus. Suspendisse tincidunt libero quis odio condimentum, et porta orci fermentum. Nulla fermentum leo neque, sit amet faucibus orci euismod vitae. Fusce sed interdum dui. Cras molestie sagittis maximus. Nullam interdum accumsan mauris eget vestibulum. Ut mollis pretium interdum. Proin malesuada eleifend lacus pulvinar luctus. ",
        likes: 95,
        liked: false,
        blogComments: [
            {name: "Jane", profileImage: "../images/jane.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "},
            {name: "Ben", profileImage: "../images/ben.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "}
        ]
    },
    {
        name: "Henry",
        blogTimestamp: "04/17/17",
        profileImage: "../images/henry.jpg",
        postContent: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel nisl quis felis placerat bibendum. Vestibulum rutrum lacus ultricies, venenatis tortor tristique, bibendum metus. Integer iaculis vel ligula ut maximus. Sed mattis massa suscipit velit efficitur, non vestibulum orci tempor. Quisque dolor velit, porta quis dignissim vel, aliquet vel lacus. Suspendisse tincidunt libero quis odio condimentum, et porta orci fermentum. Nulla fermentum leo neque, sit amet faucibus orci euismod vitae. Fusce sed interdum dui. Cras molestie sagittis maximus. Nullam interdum accumsan mauris eget vestibulum. Ut mollis pretium interdum. Proin malesuada eleifend lacus pulvinar luctus. ",
        likes: 24,
        liked: false,
        blogComments: [
            {name: "Joe", profileImage: "../images/joe.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "}
        ]
    },

    {
        name: "Joe",
        blogTimestamp: "04/08/17",
        profileImage: "../images/joe.jpg",
        postContent: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel nisl quis felis placerat bibendum. Vestibulum rutrum lacus ultricies, venenatis tortor tristique, bibendum metus. Integer iaculis vel ligula ut maximus. Sed mattis massa suscipit velit efficitur, non vestibulum orci tempor. Quisque dolor velit, porta quis dignissim vel, aliquet vel lacus. Suspendisse tincidunt libero quis odio condimentum, et porta orci fermentum. Nulla fermentum leo neque, sit amet faucibus orci euismod vitae. Fusce sed interdum dui. Cras molestie sagittis maximus. Nullam interdum accumsan mauris eget vestibulum. Ut mollis pretium interdum. Proin malesuada eleifend lacus pulvinar luctus. ",
        likes: 36,
        liked: true,
        blogComments: [
            {name: "Jane", profileImage: "../images/jane.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "},
            {name: "Ben", profileImage: "../images/ben.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "}
        ]
    },
    {
        name: "Henry",
        blogTimestamp: "03/15/17",
        profileImage: "../images/henry.jpg",
        postContent: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel nisl quis felis placerat bibendum. Vestibulum rutrum lacus ultricies, venenatis tortor tristique, bibendum metus. Integer iaculis vel ligula ut maximus. Sed mattis massa suscipit velit efficitur, non vestibulum orci tempor. Quisque dolor velit, porta quis dignissim vel, aliquet vel lacus. Suspendisse tincidunt libero quis odio condimentum, et porta orci fermentum. Nulla fermentum leo neque, sit amet faucibus orci euismod vitae. Fusce sed interdum dui. Cras molestie sagittis maximus. Nullam interdum accumsan mauris eget vestibulum. Ut mollis pretium interdum. Proin malesuada eleifend lacus pulvinar luctus. ",
        likes: 27,
        liked: true,
        blogComments: [
            {name: "Joe", profileImage: "../images/joe.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "}
        ]
    },

    {
        name: "Joe",
        blogTimestamp: "03/14/17",
        profileImage: "../images/joe.jpg",
        postContent: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel nisl quis felis placerat bibendum. Vestibulum rutrum lacus ultricies, venenatis tortor tristique, bibendum metus. Integer iaculis vel ligula ut maximus. Sed mattis massa suscipit velit efficitur, non vestibulum orci tempor. Quisque dolor velit, porta quis dignissim vel, aliquet vel lacus. Suspendisse tincidunt libero quis odio condimentum, et porta orci fermentum. Nulla fermentum leo neque, sit amet faucibus orci euismod vitae. Fusce sed interdum dui. Cras molestie sagittis maximus. Nullam interdum accumsan mauris eget vestibulum. Ut mollis pretium interdum. Proin malesuada eleifend lacus pulvinar luctus. ",
        likes: 21,
        liked: false,
        blogComments: [
            {name: "Jane", profileImage: "../images/jane.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "},
            {name: "Ben", profileImage: "../images/ben.jpg", comment: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu viverra lacus. Ut in augue fermentum, tristique justo nec, venenatis libero. Donec fringilla massa in mi laoreet, in sollicitudin diam. "}
        ]
    }
]

ReactDOM.render(<App blogPosts={blogPosts}/>, document.getElementById('root'));
registerServiceWorker();
