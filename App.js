import React, { Component } from 'react'
import { BlogFeed } from "./BloggyComponents/BlogFeed"
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App" id="App">
          <div>
              <BlogFeed blogPosts={this.props.blogPosts}/>
          </div>
      </div>
    );
  }
}

export default App;
