import React from "react"
import { BlogPost } from "./BlogPost";

export class BlogFeed extends React.Component {
    render() {
        let posts = []

        for (let i=0; i < this.props.blogPosts.length; i++) {
           posts.push(<BlogPost post={this.props.blogPosts[i]}/>)
        }

        return (
            <article className="posts">
                {posts}
            </article>
        )
    }
}
