import React from "react"
import { ProfileImage } from "./ProfileImage";

export class CommentInput extends React.Component {
    constructor(props) {
        super(props)

        this.handleUserInput = this.handleUserInput.bind(this)
        this.handleKeyPress = this.handleKeyPress.bind(this)
    }

    handleUserInput(e) {
        this.props.onChange(e)
    }

    handleKeyPress(e) {
        if (e.key === "Enter") {
            this.props.onSubmit()
        }
    }

    render() {
        return (
            <div className="commentInput">
                <ProfileImage profileImage={this.props.profileImage} />
                <input type="text" placeholder="Comment..."
                       className="sendComment"
                       onChange={this.handleUserInput}
                       value={this.props.userInput}
                       onKeyPress={this.handleKeyPress}
                />

                <input type="submit"
                       value="Send"
                       className="sendButton"
                       onClick={this.props.onSubmit}
                />
                <br className="clear" />
            </div>
        )
    }
}