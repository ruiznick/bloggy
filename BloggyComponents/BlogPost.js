import React from "react"
import { PostBox } from "./PostBox";
import { CommentBox } from "./CommentBox";

export class BlogPost extends React.Component {
    render() {
        return (
            <div className="blogPost">
                <PostBox
                    name={this.props.post.name}
                    profileImage={this.props.post.profileImage}
                    postDate={this.props.post.blogTimestamp}
                    postContent={this.props.post.postContent}
                    likes={this.props.post.likes}
                    liked={this.props.post.liked}
                />

                <CommentBox comments={this.props.post.blogComments}/>

            </div>
        )
    }
}