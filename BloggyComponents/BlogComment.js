import React from "react"
import { ProfileImage } from "./ProfileImage";

export class BlogComment extends React.Component {
    render() {
        return (
            <div className="comment">
                <ProfileImage profileImage={this.props.blogComment.profileImage}/>
                <a href="#app" className="commentor"> {this.props.blogComment.name} </a>
                <span className="userComment"> {this.props.blogComment.comment} </span>
                <br className="clear" />
            </div>
        )
    }
}