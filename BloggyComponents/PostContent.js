import React from "react"

export class PostContent extends React.Component {
    render() {
        return (
            <div className="postContent">
                <p>{this.props.postContent}</p>
            </div>
        )
    }
}