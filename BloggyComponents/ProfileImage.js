import React from "react"
import ben from "../images/ben.jpg"
import henry from "../images/henry.jpg"
import jane from "../images/jane.jpg"
import joe from "../images/joe.jpg"

export class ProfileImage extends React.Component {
    render() {
        let imgSrc = ""

        if (this.props.profileImage.includes("ben")) {
            imgSrc = ben
        } else if (this.props.profileImage.includes("henry")) {
            imgSrc = henry
        } else if (this.props.profileImage.includes("jane")) {
            imgSrc = jane
        } else if (this.props.profileImage.includes("joe")) {
            imgSrc = joe
        }

        return (
            <div className="profileImage">
                <img src={imgSrc} alt="Profile" />
            </div>
        )
    }
}