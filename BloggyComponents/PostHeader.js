import React from "react"
import { ProfileImage } from "./ProfileImage";

export class PostHeader extends React.Component {
    render() {
        return (
            <div className="postHeader">
                <ProfileImage profileImage={this.props.profileImage}/>
                <a href="#app" className="postName">
                    {this.props.name}
                </a>
                <span className="postDate">
                    {this.props.postDate}
                </span>
            </div>
        )
    }
}