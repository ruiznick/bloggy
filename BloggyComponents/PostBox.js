import React from "react"
import { PostHeader } from "./PostHeader";
import { PostContent } from "./PostContent";
import { PostFooter } from "./PostFooter";

export class PostBox extends React.Component {
    render() {
        return (
            <div className="postBox">
                <PostHeader
                    name={this.props.name}
                    postDate={this.props.postDate}
                    profileImage={this.props.profileImage}
                />

                <br className="clear" />

                <PostContent postContent={this.props.postContent}/>

                <PostFooter likes={this.props.likes}
                            liked={this.props.liked}
                />

                <br className="clear" />
            </div>
        )
    }
}