import React from "react"
import { BlogComment } from "./BlogComment";
import { CommentInput } from "./CommentInput";

export class CommentBox extends React.Component {
    constructor(props) {
        super(props)

        this.handleSubmit = this.handleSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)

        this.state = {comments: props.comments, userInput: ""}
    }

    handleSubmit() {
        let userInput = this.state.userInput
        if (userInput.length < 1) {
            return
        }

        let comments = this.state.comments
        let newComment = {
                            name: "Ben",
                            profileImage:"../images/ben.jpg",
                            comment: userInput
                         }

        comments.push(newComment)

        this.setState({comments: comments,
                        userInput: ""})
    }

    handleChange(e) {
        this.setState({userInput: e.target.value})
    }

    render() {

        let comments = []

        for (let i=0; i < this.state.comments.length; i++) {
            comments.push(<BlogComment blogComment={this.state.comments[i]} />)
        }

        return (
            <div className="commentBox">
                <div className="comments">
                    {comments}
                </div>

                <br className="clear" />

                <CommentInput profileImage="ben"
                              onSubmit={this.handleSubmit}
                              onChange={this.handleChange}
                              userInput={this.state.userInput}
                />

            </div>
        )

    }
}