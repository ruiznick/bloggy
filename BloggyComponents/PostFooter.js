import React from "react"
import like from "../images/like.svg"
import liked from "../images/liked.svg"

export class PostFooter extends React.Component {
    constructor(props) {
        super(props)

        this.handleClick = this.handleClick.bind(this)

        this.state = {liked: props.liked, likes: props.likes}
    }

    handleClick() {
        let newLike = this.state.liked ? false : true
        let newLikes = this.state.likes

        if (newLike) {
            newLikes++
        } else {
            newLikes--
        }

        this.setState({liked: newLike, likes: newLikes})
    }

    render() {
        let likeSrc = this.state.liked ? liked : like

        return (
            <div className="postFooter">
                <span>{this.state.likes}</span>
                <img src={likeSrc} alt="Like" onClick={this.handleClick}/>
            </div>
        )
    }
}